#!/bin/bash

/local/repository/ceph-common.sh

sudo ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow'
sudo sh -c "umask 177; grep \"key = \" /etc/ceph/ceph.client.admin.keyring | awk '{print $3}' > /etc/ceph/ceph.client.admin.secret"
sudo ceph-authtool --create-keyring /etc/ceph/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *' --import-keyring /etc/ceph/ceph.client.admin.keyring

MON1=`grep ceph_mon1-lan_cephfs_fe /etc/hosts | cut -f 1`
MON2=`grep ceph_mon2-lan_cephfs_fe /etc/hosts | cut -f 1`
MON3=`grep ceph_mon3-lan_cephfs_fe /etc/hosts | cut -f 1`
sudo monmaptool --create --fsid `uuidgen` --add 0 $MON1 --add 1 $MON2 --add 2 $MON3 /etc/ceph/ceph.initial-monmap

FSID_UUID=`uuidgen`

OSD1_FE=`grep ceph_osd1-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD2_FE=`grep ceph_osd2-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD3_FE=`grep ceph_osd3-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD1_BE=`grep ceph_osd1-lan_cephfs_be /etc/hosts | cut -f 1`
OSD2_BE=`grep ceph_osd2-lan_cephfs_be /etc/hosts | cut -f 1`
OSD3_BE=`grep ceph_osd3-lan_cephfs_be /etc/hosts | cut -f 1`

mkdir /local/geniuser
cd /local/geniuser

cat > ceph.conf <<END
[global]
  fsid = $FSID_UUID
  cluster = ceph
  public network = 192.168.3.0/24
  cluster network = 192.168.4.0/24

  auth cluster required = cephx
  auth service required = cephx
  auth client required = cephx

  osd pool default size = 3
  osd pool default min size = 1

[mon]
  mon host = ceph-mon-1 ceph-mon-2 ceph-mon-3
  mon addr = $MON1 $MON2 $MON3
  mon initial members = 0, 1, 2

[mon.0]
  host = ceph-mon-1
  mon addr = $MON1:6789

[mon.1]
  host = ceph-mon-2
  mon addr = $MON2:6789

[mon.2]
  host = ceph-mon-3
  mon addr = $MON3:6789

[osd]
  osd journal size = 1024
  filestore xattr use omap = true
  osd mkfs type = ext4
  osd mount options ext4 = user_xattr,rw,noatime
  osd max object name len = 256
  osd max object namespace len = 64

[osd.0]
  host = ceph-osd-0
  devs = /dev/mapper/vg_system-ceph
  public addr = $OSD1_FE
  cluster addr = $OSD1_BE

[osd.1]
  host = ceph-osd-1
  devs = /dev/mapper/vg_system-ceph
  public addr = $OSD2_FE
  cluster addr = $OSD2_BE

[osd.2]
  host = ceph-osd-2
  devs = /dev/mapper/vg_system-ceph
  public addr = $OSD3_FE
  cluster addr = $OSD3_BE

[mds.m0]
  host = ceph-mon-1

[mds.m1]
  host = ceph-mon-2

[mds.m2]
  host = ceph-mon-3
END

sudo cp ceph.conf /etc/ceph/ceph.conf
rm ceph.conf

sudo ceph-mon --mkfs -i 0 --monmap /etc/ceph/ceph.initial-monmap --keyring /etc/ceph/ceph.mon.keyring
sudo chown -R ceph:ceph /var/lib/ceph/mon

sudo systemctl enable ceph-mon@0
sudo systemctl start ceph-mon@0

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

sudo cp -p /etc/ceph/ceph.client.admin.secret /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.client.admin.keyring /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.mon.keyring /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.initial-monmap /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.conf /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/


while ! nc -z ceph_mon2 1234; do
    sleep 1
done
while ! nc -z ceph_mon3 1234; do
    sleep 1
done
while ! nc -z ceph_osd1 1234; do
    sleep 1
done
while ! nc -z ceph_osd2 1234; do
    sleep 1
done
while ! nc -z ceph_osd3 1234; do
    sleep 1
done

#sudo -u ceph mkdir -p /var/lib/ceph/mgr/ceph-0
#sudo ceph auth get-or-create mgr.0 mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/ceph-0/keyring

sudo -u ceph mkdir -p /var/lib/ceph/mds/ceph-m0
sudo sh -c "ceph auth get-or-create mds.m0 mds 'allow' osd 'allow *' mon 'allow rwx' > /var/lib/ceph/mds/ceph-m0/keyring"
sudo ceph-mds --cluster ceph -i m0 -m ceph_mon1-fe:6789

sudo systemctl enable ceph-mds@m0
sudo systemctl start ceph-mds@m0

sleep 1

sudo ceph osd pool create cephfs_data 64
sudo ceph osd pool create cephfs_metadata 64

sudo ceph fs new root_ceph cephfs_metadata cephfs_data
