#!/bin/bash

set -e

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

MON1=`grep ceph_mon1-lan_cephfs_fe /etc/hosts | cut -f 1`
MON2=`grep ceph_mon2-lan_cephfs_fe /etc/hosts | cut -f 1`
MON3=`grep ceph_mon3-lan_cephfs_fe /etc/hosts | cut -f 1`

sudo /local/repository/startup-common.sh

sudo sysctl net.bridge.bridge-nf-call-iptables=1
sudo kubeadm init --pod-network-cidr=10.244.0.0/16

sudo kubeadm token create --print-join-command > /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/kubeadm_join_cmd.sh
chmod +x /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/kubeadm_join_cmd.sh

while ! nc -z k8s-node-1 1234; do
    sleep 1
done
while ! nc -z k8s-node-2 1234; do
    sleep 1
done
while ! nc -z k8s-node-3 1234; do
    sleep 1
done

mkdir /local/geniuser
export HOME=/local/geniuser
cd $HOME
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

sudo kubectl apply -f /local/repository/calico.yaml

case `uname -m` in
x86_64)
    wget https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz
    tar xf helm-v3.0.2-linux-amd64.tar.gz
    sudo cp linux-amd64/helm /usr/local/bin/
    ;;
aarch64)
    wget https://get.helm.sh/helm-v3.0.2-linux-arm64.tar.gz
    tar xf helm-v3.0.2-linux-arm64.tar.gz
    sudo cp linux-arm64/helm /usr/local/bin/
    ;;
esac

kubectl create namespace cephfs
kubectl apply -f /local/repository/storage-class/

CEPH_KEY=`sudo sed "s/[ \t]*key = //" /proj/${PROJ_NAME}/exp/${EXP_NAME}/tmp/ceph.client.admin.secret`

cat > secret.yaml <<END
kind: Secret
apiVersion: v1
metadata:
  name: ceph-secret-admin
  namespace: cephfs
stringData:
  key: ${CEPH_KEY}
END

kubectl apply -f secret.yaml

cat > class.yaml <<END
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: cephfs
provisioner: ceph.com/cephfs
parameters:
    monitors: ${MON1}:6789,${MON2}:6789,${MON3}:6789
    adminId: admin
    adminSecretName: ceph-secret-admin
    adminSecretNamespace: "cephfs"
    claimRoot: /pvc-volumes
END

kubectl apply -f class.yaml

kubectl apply -f /local/repository/nginx-ingress-service.yaml
kubectl apply -f /local/repository/metallb/
kubectl apply -f /local/repository/registry/
