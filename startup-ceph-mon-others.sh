#!/bin/bash

/local/repository/ceph-common.sh

MON1=`grep ceph_mon1-lan_cephfs_fe /etc/hosts | cut -f 1`
MON2=`grep ceph_mon2-lan_cephfs_fe /etc/hosts | cut -f 1`
MON3=`grep ceph_mon3-lan_cephfs_fe /etc/hosts | cut -f 1`

OSD1_FE=`grep ceph_osd1-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD2_FE=`grep ceph_osd2-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD3_FE=`grep ceph_osd3-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD1_BE=`grep ceph_osd1-lan_cephfs_be /etc/hosts | cut -f 1`
OSD2_BE=`grep ceph_osd2-lan_cephfs_be /etc/hosts | cut -f 1`
OSD3_BE=`grep ceph_osd3-lan_cephfs_be /etc/hosts | cut -f 1`

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

nc -l -p 1234

sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.secret /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.mon.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.initial-monmap /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.conf /etc/ceph/

MON_IDX=$(expr `cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $1}' | sed "s/ceph_mon//"` - 1)

sudo ceph-mon --mkfs -i ${MON_IDX} --monmap /etc/ceph/ceph.initial-monmap --keyring /etc/ceph/ceph.mon.keyring
sudo chown -R ceph:ceph /var/lib/ceph/mon

sudo systemctl enable ceph-mon@${MON_IDX}
sudo systemctl start ceph-mon@${MON_IDX}

