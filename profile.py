"""An example of constructing a profile with two physical nodes connected by a Link.

Instructions:
Wait for the profile instance to start, and then log in to either host.
"""

import geni.portal as portal
import geni.rspec.pg as rspec

pc = portal.Context()

pc.defineParameter("hw_type", "Hardware Type",
                   portal.ParameterType.STRING, "d710",
                   (
                    ("pc3000", "Emulab pc3000 nodes 2x1-core 2GB RAM"),
                    ("d710", "Emulab d710 nodes 1x4-core 12GB RAM"),
                    ("d430", "Emulab d430 nodes 2x8-core 64GB RAM"),
                    ("d740", "POWDER d740 nodes 2x16-core 96 GB RAM"),
                    ("d820", "Emulab d820 nodes 4x8-core 128 GB RAM"),
                    ("m400", "CloudLab m400 nodes 1x8 ARMv8 64 GB RAM"),
                   ),
                   longDescription="Node resource type"
                  )

params = pc.bindParameters()
pc.verifyParameters()

request = portal.context.makeRequestRSpec()

# Create two raw "PC" nodes
node_k8s_master_1 = request.RawPC("k8s-master-1")
#node_k8s_master_2 = request.RawPC("k8s-master-2")
#node_k8s_master_3 = request.RawPC("k8s-master-3")
node_k8s_master_1.addService(rspec.Execute(shell="bash", command="/local/repository/startup-master.sh"))
#node_k8s_master_2.addService(rspec.Execute(shell="bash", command="/local/repository/startup-master.sh"))
#node_k8s_master_3.addService(rspec.Execute(shell="bash", command="/local/repository/startup-master.sh"))
node_k8s_node_1 = request.RawPC("k8s-node-1")
node_k8s_node_2 = request.RawPC("k8s-node-2")
node_k8s_node_3 = request.RawPC("k8s-node-3")
node_k8s_node_1.addService(rspec.Execute(shell="bash", command="/local/repository/startup-node.sh"))
node_k8s_node_2.addService(rspec.Execute(shell="bash", command="/local/repository/startup-node.sh"))
node_k8s_node_3.addService(rspec.Execute(shell="bash", command="/local/repository/startup-node.sh"))

# mon
node_ceph_mon1 = request.RawPC("ceph_mon1")
node_ceph_mon2 = request.RawPC("ceph_mon2")
node_ceph_mon3 = request.RawPC("ceph_mon3")
node_ceph_mon1.addService(rspec.Execute(shell="bash", command="/local/repository/startup-ceph-mon.sh"))
node_ceph_mon2.addService(rspec.Execute(shell="bash", command="/local/repository/startup-ceph-mon-others.sh"))
node_ceph_mon3.addService(rspec.Execute(shell="bash", command="/local/repository/startup-ceph-mon-others.sh"))

# osd
node_ceph_osd1 = request.RawPC("ceph_osd1")
node_ceph_osd2 = request.RawPC("ceph_osd2")
node_ceph_osd3 = request.RawPC("ceph_osd3")
node_ceph_osd1.addService(rspec.Execute(shell="bash", command="/local/repository/startup-ceph-osd.sh"))
node_ceph_osd2.addService(rspec.Execute(shell="bash", command="/local/repository/startup-ceph-osd.sh"))
node_ceph_osd3.addService(rspec.Execute(shell="bash", command="/local/repository/startup-ceph-osd.sh"))


link_1 = request.LAN("lan_metallb")
link_2 = request.LAN("lan_k8s")
link_3 = request.LAN("lan_cephfs_fe")
link_4 = request.LAN("lan_cephfs_be")

for link in (link_1, link_2, link_3, link_4):
    link.bandwidth = 1000*1000
    if params.hw_type == "m400":
        link.link_multiplexing = True
        link.vlan_tagging = True


all_nodes = [node_k8s_master_1,
             #node_k8s_master_2,
             #node_k8s_master_3,
             node_k8s_node_1,
             node_k8s_node_2,
             node_k8s_node_3,
             node_ceph_mon1,
             node_ceph_mon2,
             node_ceph_mon3,
             node_ceph_osd1,
             node_ceph_osd2,
             node_ceph_osd3,
            ]


for i, node in enumerate(all_nodes):
    node.hardware_type = params.hw_type
    # Need this because ceph provisioner has problems with old kernel https://github.com/kubernetes-incubator/external-storage/issues/345
    #node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    # Need >= 2020 to get rtslib-fb to a new enough version for ceph-iscsi
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"


    # Fake external network for MetalLB
    iface_1 = node.addInterface("if1")
    iface_1.addAddress(rspec.IPv4Address("192.168.1.%d"%(i+1),
                                         "255.255.255.0"))

    # Internal network for k8s
    iface_2 = node.addInterface("if2")
    iface_2.addAddress(rspec.IPv4Address("192.168.2.%d"%(i+1),
                                         "255.255.255.0"))

    # Internal network for cephfs
    iface_3 = node.addInterface("if3")
    iface_3.addAddress(rspec.IPv4Address("192.168.3.%d"%(i+1),
                                         "255.255.255.0"))

    # Internal network for cephfs
    iface_4 = node.addInterface("if4")
    iface_4.addAddress(rspec.IPv4Address("192.168.4.%d"%(i+1),
                                         "255.255.255.0"))

    link_1.addInterface(iface_1)
    link_2.addInterface(iface_2)
    link_3.addInterface(iface_3)
    link_4.addInterface(iface_4)


portal.context.printRequestRSpec()
