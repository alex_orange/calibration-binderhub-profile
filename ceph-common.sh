#!/bin/bash

wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
sudo apt-add-repository "deb https://download.ceph.com/debian-nautilus $(lsb_release -sc) main"
sudo apt-get update
sudo apt-get install -y ceph ceph-mds
