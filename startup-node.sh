#!/bin/bash

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

sudo /local/repository/startup-common.sh

nc -l -p 1234

sudo /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/kubeadm_join_cmd.sh
