#!/bin/bash

/local/repository/ceph-common.sh

MON1=`grep ceph_mon1-lan_cephfs_fe /etc/hosts | cut -f 1`
MON2=`grep ceph_mon2-lan_cephfs_fe /etc/hosts | cut -f 1`
MON3=`grep ceph_mon3-lan_cephfs_fe /etc/hosts | cut -f 1`

OSD1_FE=`grep ceph_osd1-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD2_FE=`grep ceph_osd2-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD3_FE=`grep ceph_osd3-lan_cephfs_fe /etc/hosts | cut -f 1`
OSD1_BE=`grep ceph_osd1-lan_cephfs_be /etc/hosts | cut -f 1`
OSD2_BE=`grep ceph_osd2-lan_cephfs_be /etc/hosts | cut -f 1`
OSD3_BE=`grep ceph_osd3-lan_cephfs_be /etc/hosts | cut -f 1`

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

nc -l -p 1234

sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.secret /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.mon.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.initial-monmap /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.conf /etc/ceph/

sudo /usr/local/etc/emulab/mkextrafs.pl -v vg_system -m ceph -l
sudo mkfs.ext4 /dev/mapper/vg_system-ceph
OSD_IDX=$(expr `cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $1}' | sed "s/ceph_osd//"` - 1)
osd_uuid=`uuidgen`
osd_id=`sudo ceph osd create ${osd_uuid} ${OSD_IDX}`
sudo -u ceph mkdir -p /var/lib/ceph/osd/ceph-${OSD_IDX}
sudo sh -c "sudo echo \"/dev/mapper/vg_system-ceph /var/lib/ceph/osd/ceph-${OSD_IDX} ext4 defaults 0 0\" >> /etc/fstab"
sudo mount /dev/mapper/vg_system-ceph

sudo ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-${OSD_IDX}/keyring --name osd.${OSD_IDX} --add-key `ceph-authtool --gen-print-key`
sudo ceph auth add osd.${OSD_IDX} osd 'allow *' mon 'allow profile osd' -i /var/lib/ceph/osd/ceph-${OSD_IDX}/keyring
sudo ceph-osd -i ${osd_id} --mkfs --osd-uuid ${osd_uuid}
sudo ceph osd crush add-bucket ceph-osd-${OSD_IDX} host
sudo ceph osd crush move ceph-osd-${OSD_IDX} root=default
sudo ceph osd crush add osd.${OSD_IDX} 1.0 host=ceph-osd-${OSD_IDX}

sudo chown ceph:ceph -R /var/lib/ceph/osd/ceph-${osd_id}
sudo systemctl enable ceph-osd@${OSD_IDX}
sudo systemctl start ceph-osd@${OSD_IDX}
